var http = require("http");
var handle = require("./handle");

var events = require('events');
var emiter = new events.EventEmitter();


// emiter.on('say_something', say);

// function say() {
//     console.log('Im saying...');
// }

// emiter.emit('say_something');

var server = http.createServer(handle.fn);

server.listen(3000, function () {
    console.log('Server is listening as port 3000');
});