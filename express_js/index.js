var express = require('express');

var app = express();

app.get('/', function (req, res) {
    res.send('Hello express!!');
});

app.get('/hello', function (req, res) {
    res.json({
        message: "This is my router hello",
        framework: 'expressjs.com'
    });
});

app.get('/hello/:name', function (req, res) {
    res.send('Welcome ' + req.params.name + '!');
});s

app.listen(3000, function () {
    console.log('Express has been started!');
});